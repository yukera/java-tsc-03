# TASK MANAGER

SCREENSHOTS

https://yadi.sk/d/RQC59RUiG2QOBQ

## DEVELOPER INFO

name: Yuliya Arinchekhina

e-mail: arin-akira@mail.ru

## HARDWARE REQUIREMENTS

CPU: i7-8750H

RAM: 16 GB

SSD: 1 TB

## SOFRWARE REQUIREMENTS

System: Windows 10 Home, version 2004

Version JDK: 1.8.0-272

## PROGRAM RUN
```
java -jar ./task-manager.jar
```
