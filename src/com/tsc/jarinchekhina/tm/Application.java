package com.tsc.jarinchekhina.tm;

import com.tsc.jarinchekhina.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yuliya Arinchekhina");
        System.out.println("E-mail: arin-akira@mail.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - display developer info");
        System.out.println(TerminalConst.VERSION + " - display program version");
        System.out.println(TerminalConst.HELP + " - display list of commands");
    }

}